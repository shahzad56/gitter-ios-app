import Foundation

import Foundation
import CoreData

class JsonToDatabaseHelper {
    typealias Callback = (_ error: Error?, _ didSaveNewData: Bool) -> Void

    private let coreData: CoreDataSingleton
    private let workerContext: NSManagedObjectContext
    private let jsonToDatabase: JsonToDatabase

    init() {
        coreData = CoreDataSingleton.sharedInstance
        workerContext = coreData.workerManagedObjectContext
        jsonToDatabase = JsonToDatabase(context: workerContext)
    }

    func upsertRoom(_ json: JsonObject, completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.upsertRoom(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func upsertRooms(_ roomsJson: [JsonObject], completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                let _ = try self.jsonToDatabase.upsertRooms(roomsJson)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func upsertRoomInUserRoomList(_ json: JsonObject, completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.upsertRoomInUserRoomList(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func patchRoom(_ json: JsonObject, completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.patchRoom(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func removeRoomFromUserRoomList(_ json: JsonObject, completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.removeRoomFromUserRoomList(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }


    func updateUserRoomList(_ roomsJson: [JsonObject], completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.updateUserRoomList(roomsJson)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func updateRoomList(_ roomsJson: [JsonObject], forGroupId groupId: String, completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.updateRoomList(roomsJson, forGroup: groupId)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func upsertUsers(_ json: [JsonObject], completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.upsertUsers(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func updateSuggestions(_ json: [JsonObject], completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.updateSuggestions(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func upsertGroup(_ json: JsonObject, completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.upsertGroup(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func updateAdminGroupList(_ json: [JsonObject], completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.updateAdminGroupList(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    func updateUserGroupList(_ json: [JsonObject], completionHandler: @escaping Callback) {
        workerContext.perform {
            do {
                try self.jsonToDatabase.updateUserGroupList(json)
                self.coreData.saveAll({ (error, didSaveNewData) in
                    self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: didSaveNewData)
                })
            } catch {
                self.callbackOnMainThread(completionHandler, error: error, didSaveNewData: false)
            }
        }
    }

    private func callbackOnMainThread(_ completionHandler: @escaping Callback, error: Error?, didSaveNewData: Bool) {
        DispatchQueue.main.async {
            completionHandler(error, didSaveNewData)
        }
    }
}
