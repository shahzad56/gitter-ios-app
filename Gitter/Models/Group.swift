import Foundation
import CoreData

class Group: NSManagedObject {
    @NSManaged var id: String
    @NSManaged var uri: String
    @NSManaged var name: String
    @NSManaged var avatarUrl: String

    @NSManaged var backedBy_type: String?
    @NSManaged var backedBy_linkPath: String?

    @NSManaged var rooms: Set<Room>
    @NSManaged var userGroupCollection: UserGroupCollection?
    @NSManaged var adminGroupCollection: AdminGroupCollection?
}

extension Group {
    var backedByType: GroupBackedByType? {
        if let backedBy_type = backedBy_type {
            return GroupBackedByType(rawValue: backedBy_type)
        } else {
            return nil
        }
    }

    var githubGroupName: String? {
        get {
            if backedByType == .GithubOrg || backedByType == .GithubUser {
                return backedBy_linkPath
            } else {
                return nil
            }
        }
    }
}

enum GroupBackedByType: String {
    case GithubOrg = "GH_ORG"
    case GithubUser = "GH_USER"
    case GithubRepo = "GH_REPO"
}
